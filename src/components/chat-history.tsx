"use client";

import { cn } from "@/lib/utils";
import { Chat } from "@prisma/client";
import Image from "next/image";
import React from "react";
import { Avatar, AvatarFallback, AvatarImage } from "./ui/avatar";

export default function ChatHistory({
  chats,
}: {
  chats: Chat & Record<string, any>[];
}) {
  const lastElement = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    lastElement.current?.scrollIntoView({ behavior: "smooth" });
  }, [chats]);

  return (
    <div>
      <div className="flex flex-col gap-2 relative">
        {chats.map((chat, index) => (
          <div
            key={index}
            className={cn(
              "flex gap-2 flex-col",
              chat.from === "USER" ? "items-end" : "items-start",
              index === chats.length - 1 && "mb-10"
            )}
            ref={index === chats.length - 1 ? lastElement : undefined}
          >
            <div
              className={cn(
                "flex gap-2 max-w-[80%]",
                chat.from === "USER" ? "flex-row-reverse" : "flex-row"
              )}
            >
              <Avatar
                className={cn(
                  "w-10 h-10 hidden md:flex",
                  chats[index - 1]?.from === chat.from && "md:hidden"
                )}
              >
                <AvatarImage src="https://github.com/shadcn.png" />
                <AvatarFallback>CN</AvatarFallback>
              </Avatar>
              <div
                className={cn(
                  "p-2 rounded-md",
                  chat.from === "USER"
                    ? "bg-blue-500 text-white"
                    : "bg-gray-200",
                  chats[index - 1]?.from === chat.from
                    ? chat.from === "USER"
                      ? "md:mr-12"
                      : "md:ml-12"
                    : ""
                )}
              >
                {typeof chat.message == "string" ? (
                  <>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: chat.message
                          .split("\n")
                          .join("<br />")
                          .replace("*", ""),
                      }}
                    />
                    {chat.imageUrl && (
                      <div className="relative w-full flex items-center justify-center">
                        <Image
                          src={chat.imageUrl}
                          className="rounded object-contain"
                          width={chat.isLoading ? 60 : 500}
                          height={chat.isLoading ? 60 : 500}
                          alt="Image"
                          unoptimized
                        />
                      </div>
                    )}
                  </>
                ) : (
                  chat.message
                )}
              </div>
            </div>
          </div>
        ))}
      </div>
      {/* <div className="absolute bottom-0 w-full flex items-center justify-center">
        <Button
          onClick={() =>
            lastElement.current?.scrollIntoView({ behavior: "smooth" })
          }
        >
          <MoveDown />
        </Button>
      </div> */}
    </div>
  );
}

