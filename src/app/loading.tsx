export default function PageLoading() {
  return (
    <div className="flex items-center justify-center h-screen">
      <div className="w-16 h-16 border-4 border-t-[#3498db] rounded-full animate-spin"></div>
    </div>
  );
}
