"use client";

import ChatHistory from "@/components/chat-history";
import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { ScrollArea } from "@/components/ui/scroll-area";
import { ActionResponse } from "@/lib/types";
import { cn } from "@/lib/utils";
import { zodResolver } from "@hookform/resolvers/zod";
import { Chat, Prisma } from "@prisma/client";
import { CircleStop, Send } from "lucide-react";
import React from "react";
import { useForm } from "react-hook-form";
import { toast } from "sonner";
import { z } from "zod";
import PageLoading from "./loading";
import { sendChatSchema } from "./schema";

type SessionWithMessage = Prisma.ChatSessionGetPayload<{
  include: {
    messages: true;
  };
}>;

export default function HomeClient() {
  const [isPending, setIsPending] = React.useState(false);
  const [controller, setController] = React.useState<AbortController | null>(
    new AbortController()
  );
  const [sessionId, setSessionId] = React.useState<string>("");
  const [chatHistory, setChatHistory] = React.useState<Chat[]>([]);
  const form = useForm<z.infer<typeof sendChatSchema>>({
    resolver: zodResolver(sendChatSchema),
    defaultValues: {
      message: "",
    },
  });

  async function getSession(sessionId: string) {
    const response = await fetch(`/api/session?sessionId=${sessionId}`);
    const { status, data, message } =
      (await response.json()) as ActionResponse<SessionWithMessage>;
    if (!response.ok) {
      toast.error(message);
      return;
    }
    if (data?.messages) {
      setChatHistory(data.messages);
    } else {
      generateSession();
    }
  }

  async function generateSession() {
    const response = await fetch("/api/session", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: "anonymous",
      }),
    });
    const { status, data, message } =
      (await response.json()) as ActionResponse<SessionWithMessage>;
    if (!response.ok) {
      toast.error(message);
      return;
    }
    if (data) {
      window.localStorage.setItem("sessionId", data.sessionId);
      setChatHistory(data.messages);
      setSessionId(data.sessionId);
    }
  }

  React.useEffect(() => {
    if (typeof window != undefined) {
      const sessionId = window.localStorage.getItem("sessionId");
      if (sessionId) {
        setSessionId(sessionId);
        getSession(sessionId);
      } else {
        generateSession();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleReject = React.useCallback(() => {
    if (controller) {
      controller.abort("Dibatalkan oleh pengguna");
      const newController = new AbortController();
      setController(newController);
      setChatHistory((prev) => {
        const lastChat = prev[prev.length - 1];
        if (lastChat.from === "BOT") {
          return prev.slice(0, prev.length - 1);
        }
        return prev;
      });
    }
  }, [controller]);

  async function handleSubmit(values: z.infer<typeof sendChatSchema>) {
    form.reset();
    setIsPending(true);
    try {
      let signal: AbortController["signal"];
      if (controller) {
        signal = controller.signal;
      } else {
        const newController = new AbortController();
        signal = newController.signal;
        setController(newController);
      }

      setChatHistory((prev) => [
        ...prev,
        {
          from: "USER",
          message: values.message,
        } as Chat,
      ]);

      const response = await fetch("/api/chat", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          ...values,
          sessionId: sessionId,
        }),
        signal: signal,
      });

      if (!response.ok) {
        const { status, message } =
          (await response.json()) as ActionResponse<undefined>;
        if (status == "error") {
          throw new Error(message);
        }
        throw new Error("Terjadi kesalahan");
      }

      const textDecoder = new TextDecoder();

      setChatHistory((prev) => [
        ...prev,
        {
          from: "BOT",
          message: "",
        } as any,
      ]);
      if (response.body != null) {
        const reader = response.body.getReader();

        while (true) {
          const { value, done } = await reader.read();
          if (done) {
            break;
          }

          const delimiter = "\u001E";
          const decodedString = textDecoder.decode(value);
          const stringOfChunks = decodedString.split(delimiter);
          for (const chunkString of stringOfChunks) {
            if (chunkString) {
              try {
                const chunk = JSON.parse(chunkString);
                if (chunk.type === "bot") {
                  setChatHistory((prev) => {
                    const lastChat = prev[prev.length - 1];
                    if (lastChat.from === "BOT") {
                      if (typeof lastChat.message != "string")
                        lastChat.message = "";
                      return prev.slice(0, prev.length - 1).concat({
                        from: "BOT",
                        message: lastChat.message + chunk.message,
                        imageUrl: chunk.image,
                      } as Chat);
                    }
                    return prev;
                  });
                } else if (chunk.type == "info") {
                  if (chunk.message == "processing") {
                    setChatHistory((prev) => {
                      const lastChat = prev[prev.length - 1];
                      if (lastChat.from === "BOT") {
                        return prev.slice(0, prev.length - 1).concat({
                          from: "BOT",
                          message: "",
                          imageUrl: "/chat-loading.gif",
                          isLoading: true,
                        } as any);
                      }
                      return prev;
                    });
                  }
                }
              } catch (error) {
                throw new Error("Terjadi kesalahan dalam parsing data");
              }
            }
          }
        }
      }
      setIsPending(false);
      form.setFocus("message");
    } catch (error: any) {
      setIsPending(false);
      setChatHistory((prev) => {
        const lastChat = prev[prev.length - 1];
        if (lastChat.from === "BOT") {
          return prev.slice(0, prev.length - 1);
        }
        return prev;
      });
      if (typeof error == "string") {
        toast.error(error);
      } else {
        if (error.name === "AbortError") {
          toast.error("Dibatalkan oleh pengguna");
          return;
        }
        toast.error(error.message);
      }
      form.setFocus("message");
    }
  }

  if (!sessionId) {
    return <PageLoading />;
  }

  return (
    <div className="flex flex-col h-screen w-full md:max-w[600px] lg:max-w-[800px] justify-between gap-4 py-10 px-1 overflow-y-hidden">
      <div>
        <h1 className="text-2xl font-bold">Chat</h1>
      </div>
      <div>
        <ScrollArea className="h-[80vh] w-full rounded-md border p-2 md:p-4">
          <ChatHistory chats={chatHistory as any} />
        </ScrollArea>
      </div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(handleSubmit)} className="flex gap-2">
          <FormField
            control={form.control}
            name="message"
            render={({ field }) => (
              <FormItem className="w-full">
                <FormMessage />
                <FormControl>
                  <Input
                    placeholder="Pertanyaan..."
                    {...field}
                    disabled={isPending}
                  />
                </FormControl>
              </FormItem>
            )}
          />
          <div className="flex items-end">
            <Button
              type="submit"
              disabled={isPending}
              className={cn(isPending && "hidden")}
            >
              {isPending ? <CircleStop /> : <Send />}
            </Button>
            <Button
              type="button"
              onClick={() => handleReject()}
              variant="destructive"
              className={cn(!isPending && "hidden")}
            >
              <CircleStop />
            </Button>
          </div>
        </form>
      </Form>
    </div>
  );
}

