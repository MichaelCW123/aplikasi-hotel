import { object, string } from "zod";

export const sendChatSchema = object({
  sessionId: string().optional(),
  message: string()
    .min(1, "Pesan tidak boleh kosong")
    .max(1000, "Pesan terlalu panjang, maksimal 1000 karakter"),
});

