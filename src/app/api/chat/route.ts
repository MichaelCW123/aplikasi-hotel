import { sendChatSchema } from "@/app/schema";
import MyChatMessageHistory from "@/lib/chat-history";
import createChain from "@/lib/langchain";
import prisma from "@/lib/prisma";
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const reqBody = await req.json();

    const { success, error, data } = sendChatSchema.safeParse(reqBody);

    if (!success) {
      return NextResponse.json(
        { status: "error", message: error.message },
        { status: 400 }
      );
    } else if (!data || !data.sessionId) {
      return NextResponse.json(
        { status: "error", message: "Data tidak ditemukan" },
        { status: 400 }
      );
    }

    const delimiter = "\u001E";
    const encoder = new TextEncoder();
    const chatHistory = new MyChatMessageHistory({
      sessionId: data.sessionId,
      prisma: prisma,
    });
    const chain = await createChain(chatHistory);
    await chatHistory.addUserMessage(data.message);
    const stream = await chain.streamEvents(
      { input: data.message },
      { version: "v1" }
    );

    const readableStream = new ReadableStream({
      async start(controller) {
        let botResult = "";
        for await (const event of stream) {
          const eventType = event.event;
          if (eventType === "on_chain_start") {
            controller.enqueue(
              encoder.encode(
                JSON.stringify({
                  type: "info",
                  message: "processing",
                }) + delimiter
              )
            );
            if (event.name === "MeruHotelAssistant") {
              console.log("\n-----");
              console.log(
                `Starting agent: ${event.name} with input: ${JSON.stringify(
                  event.data.input
                )}`
              );
            }
          } else if (eventType === "on_chain_end") {
            if (event.name === "MeruHotelAssistant") {
              console.log("\n-----");
              console.log(`Finished agent: ${event.name}\n`);
              console.log(`Agent output was: ${event.data.output}`);
              console.log("\n-----");
              if (botResult) {
                await chatHistory.addAIChatMessage(botResult);
              }
              return controller.close();
            }
          } else if (eventType === "on_llm_stream") {
            const content = event.data?.chunk?.message?.content as string;
            if (content !== undefined && content !== "") {
              botResult += content;
              controller.enqueue(
                encoder.encode(
                  JSON.stringify({
                    type: "bot",
                    message: content,
                  }) + delimiter
                )
              );
            }
          } else if (eventType === "on_tool_start") {
            console.log("\n-----");
            console.log(
              `Starting tool: ${event.name} with inputs: ${event.data.input}`
            );
          } else if (eventType === "on_tool_end") {
            console.log("\n-----");
            console.log(`Finished tool: ${event.name}\n`);
            console.log(`Tool output was: ${event.data.output}`);
            console.log("\n-----");
          }
        }
      },
    });

    return new Response(readableStream);
  } catch (error) {
    console.log(error);
    return NextResponse.json(
      { status: "error", message: "Terjadi kesalahan" },
      { status: 500 }
    );
  }
}

