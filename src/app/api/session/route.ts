import prisma from "@/lib/prisma";
import crypto from "crypto";
import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest, res: NextResponse) {
  try {
    const sessionId = req.nextUrl.searchParams.get("sessionId") as
      | string
      | undefined;

    if (!sessionId) {
      return NextResponse.json(
        {
          status: "error",
          message: "Session ID harus dikirim",
        },
        { status: 400 }
      );
    }

    const session = await prisma.chatSession.findUnique({
      where: {
        sessionId,
      },
      include: {
        messages: {
          orderBy: {
            createdAt: "asc",
          },
        },
      },
    });

    return NextResponse.json({
      status: "success",
      message: "Session berhasil didapatkan",
      data: session,
    });
  } catch (error) {
    return NextResponse.json(
      { status: "error", message: "Terjadi kesalahan" },
      { status: 500 }
    );
  }
}

export async function POST(req: NextRequest, res: NextResponse) {
  try {
    const reqBody = await req.json();

    const sessionId = crypto.randomUUID();

    await prisma.chatSession.create({
      data: {
        sessionId,
        messages: {
          create: {
            message: `Selamat datang di chatbot Hotel Meru! 👋\n\nKami siap membantu Anda merencanakan pengalaman menginap yang tak terlupakan di hotel kami. Apakah Anda ingin mencari informasi tentang kamar, fasilitas, atau promo spesial kami? Atau mungkin Anda ingin langsung melakukan reservasi? Jangan ragu untuk bertanya, kami akan dengan senang hati membantu Anda! 😊`,
            from: "BOT",
          },
        },
      },
    });

    const session = await prisma.chatSession.findUnique({
      where: {
        sessionId,
      },
      include: {
        messages: {
          orderBy: {
            createdAt: "asc",
          },
        },
      },
    });

    return NextResponse.json({
      status: "success",
      message: "Session berhasil dibuat",
      data: session,
    });
  } catch (error) {
    return NextResponse.json(
      { status: "error", message: "Terjadi kesalahan" },
      { status: 500 }
    );
  }
}

