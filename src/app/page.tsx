import HomeClient from "./client";

export default async function Home({
  params,
  searchParams,
}: {
  params: any;
  searchParams: { sessionId?: string };
}) {
  // await insertDummyData();
  return (
    <main className="container min-h-screen md:flex md:justify-center">
      <HomeClient />
    </main>
  );
}

