import { BaseChatMessageHistory } from "@langchain/core/chat_history";
import {
  AIMessage,
  BaseMessage,
  HumanMessage,
  SystemMessage,
} from "@langchain/core/messages";
import { Chat, ChatFrom, PrismaClient } from "@prisma/client";

export default class MyChatMessageHistory extends BaseChatMessageHistory {
  private chats: BaseMessage[] = [];
  public lc_namespace = ["langchain", "stores", "message", "postgres"];
  public sessionId = "";
  public prisma: PrismaClient | null = null;

  constructor({
    sessionId,
    prisma,
  }: {
    sessionId: string;
    prisma: PrismaClient;
  }) {
    super();
    this.sessionId = sessionId;
    this.prisma = prisma;
  }

  chatToBaseMessage(chat: Chat): BaseMessage {
    if (chat.from == "USER") {
      return new HumanMessage(chat.message);
    } else if (chat.from == "BOT") {
      return new AIMessage(chat.message);
    } else {
      return new SystemMessage(chat.message);
    }
  }

  async getMessages() {
    if (!this.prisma) {
      throw new Error("Prisma not initialized");
    }
    const session = await this.prisma.chatSession.findUnique({
      where: {
        sessionId: this.sessionId,
      },
      include: {
        messages: {
          orderBy: {
            createdAt: "asc",
          },
        },
      },
    });

    if (!session) {
      await this.prisma.chatSession.create({
        data: {
          sessionId: this.sessionId,
        },
      });
      return [];
    }

    return session.messages.map(this.chatToBaseMessage);
  }

  async addMessage(
    message: HumanMessage | AIMessage | SystemMessage,
    {
      imageUrl,
    }: {
      imageUrl?: string;
    } = {}
  ) {
    if (!this.prisma) {
      throw new Error("Prisma not initialized");
    }

    let from: ChatFrom;

    if (message instanceof HumanMessage) {
      from = "USER";
    } else if (message instanceof AIMessage) {
      from = "BOT";
    } else {
      from = "SYSTEM";
    }

    await this.prisma.chat.create({
      data: {
        session: {
          connectOrCreate: {
            where: {
              sessionId: this.sessionId,
            },
            create: {
              sessionId: this.sessionId,
            },
          },
        },
        from: from,
        message: message.content.toString(),
        imageUrl: imageUrl,
      },
      include: {
        session: true,
      },
    });
  }

  async addUserMessage(message: string) {
    return await this.addMessage(new HumanMessage(message));
  }

  async addAIChatMessage(message: string, imageUrl?: string) {
    return await this.addMessage(new AIMessage(message), { imageUrl });
  }

  async addSysteMessage(message: string) {
    return await this.addMessage(new SystemMessage(message));
  }

  async addMessages(messages: (HumanMessage | AIMessage | SystemMessage)[]) {
    for (const message of messages) {
      await this.addMessage(message);
    }
  }
  async clear() {
    await this.prisma?.chatSession.delete({
      where: {
        sessionId: this.sessionId,
      },
    });
  }
}

