import { Chroma } from "@langchain/community/vectorstores/chroma";
import { BaseChatMessageHistory } from "@langchain/core/chat_history";
import {
  ChatPromptTemplate,
  MessagesPlaceholder,
} from "@langchain/core/prompts";
import { RunnableSequence } from "@langchain/core/runnables";
import { DynamicTool } from "@langchain/core/tools";
import { ChatOpenAI, OpenAIEmbeddings } from "@langchain/openai";
import { AgentExecutor, createOpenAIFunctionsAgent } from "langchain/agents";
import { SqlToolkit } from "langchain/agents/toolkits/sql";
import { BufferWindowMemory } from "langchain/memory";
import { MultiQueryRetriever } from "langchain/retrievers/multi_query";
import { SqlDatabase } from "langchain/sql_db";
import { pgDataSource } from "./typeorm";

const model = new ChatOpenAI({
  temperature: 0,
  maxConcurrency: 10,
  modelName: "gpt-3.5-turbo",
  streaming: true,
  // verbose: true,
});

async function createMeruHotelAgent(chatHistory: BaseChatMessageHistory) {
  const embeddings = new OpenAIEmbeddings();
  const vectorStore = new Chroma(embeddings, {
    collectionName: "hotel_database",
    url: "http://128.199.255.161:8002",
  });

  const prompt = `
Today : ${new Date().toLocaleDateString("id-ID")}
FOR HOTEL MERU INFORMATION:
You are a versatile agent tasked with handling customer inquiries about the Meru Sanur Hotel and generating SQL database queries. Your role involves answering questions in Bahasa Indonesia and creating syntactically correct SQL queries based on the dialect specified by the user. When answering hotel inquiries, provide clear, informative, and professional responses tailored to each question, relying on the retriever tool and database information only. For SQL query tasks, construct and execute queries to fetch specific data, ensuring to limit the results as per the user's request and to only query relevant columns. Always verify your SQL queries to prevent errors.

Your responses to hotel inquiries should be friendly and engaging, reflecting the high standards expected of a customer service assistant at Meru Sanur Hotel. When creating SQL queries, ensure they are precise and adhere strictly to user requirements, including result limits and specific data fields.

If a question does not relate to the hotel or the database, respond with "I don't know" for database queries and "I don't know" for hotel inquiries. Always maintain professionalism, courtesy, and a helpful demeanor in all interactions, whether answering questions about the hotel or handling database queries.

The answers provided do not have to follow the format given in the examples exactly but can use different wording to sound more natural and human-like. The responses should convey the same meaning but can be expressed in various ways to avoid sounding repetitive. Do not use symbols like ### or ** in your responses. 

IMPORTANT: All responses must be based on data retrieved from the retriever tool and retriever database tool. Do not use any other sources for data.

FOR HOTEL MERU ROOM AVAILBLE
Existing database Schema:
table name: "room_type" - Contains information about different room types.
table column:
  - id
  - name
  - daily_price
  - total_room
  - description
  - image_url

table name: "room"
table column:
  - id
  - number
  - max_guests
  - type_id (References room_type)

table name: "booking"
table column:
  - id
  - room_id (References room)
  - check_in (timestamp)
  - check_out (timestamp)
  - status

Relationships:
A room_type can have many rooms.
Each room belongs to one room_type.
A room can have many bookings.
Each booking corresponds to one room.

DO NOT USE INSERT, UPDATE, DELETE query, only use SELECT

  ## Here are some example ##
        <Example>
        1.  question: jenis kamar apa saja yang ada?
            answer: 
                    1. Meru Suite at The Meru Sanur
                      Details Meru Room
                      An expansive comfort of our Meru Suite, featuring a separate living room with a terrace or balcony overlooking the tranquil Sanur Beach and garden. With a generous 160 square meters of space, this suite offers an exceptional living experience.

                    2. Premier Suite at The Meru Sanur
                      Details Premier Room
                      Experience our Premier Suites, feature 125 square meters of space and a stunning view of our tranquil garden. Each suite is adorned with refined furnishings includes a private balcony or terrace, providing the ultimate of luxurious resort-style living.

                    3. Deluxe Suite at The Meru Sanur
                      Details Deluxe Room
                      Experience the luxury of our Deluxe Suite, a spacious 83-square-meter retreat features a private balcony or expansive terrace overlooking our serene Sanur beach or tropical garden.

                    4. Corner Suite
                      Details Corner Room
                      Indulge in the expansive comfort of our Corner Suite, featuring serene garden views. Offering a spacious 98 square meters, this suite includes a private balcony or terrace and a dedicated dining table for your convenience.

                    5. Presidential Suite
                      Details Presidential Room
                      Experience the epitome of luxury with our Presidential Suites, the most prestigious accommodation at the resort. These extensive 209 square meters, promises an extraordinary retreat, ensuring an unparalleled experience.
 
        
        2.  question: dimana lokasi hotel?
            answer: JALAN HANG TUAH
                    SANUR KAJA, DENPASAR SELATAN,
                    KOTA DENPASAR, BALI 80227
        
        3.  question: fasilitas apa saja yang tersedia?
            answer: 1. Svasana Spa
                        Step into our Reawakening Treatment Spa, a haven where timeless practices harmonize with contemporary comfort. Delight in a rejuvenating journey that revitalizes the body, mind, and spirit. Awaken to a heightened

                    2. Holistic
                        Experience the profound impact of holistic well-being: submerge yourself in sound therapy, align your chakras, and let go of emotional weight for a deeply revitalizing journey Spiritual
                        Begin a transformative Balinese spiritual odyssey: surrender to the insights of sightless healers, luxuriate in lukat gni and lukat toya ceremonies, discover inner serenity through mebayuh, and contemplate profoundly with metenung

                    3. Active Wellness
                        Renew both body and mind through our invigorating active wellness program. Engage in revitalizing yoga, energize with cardio boxing, conquer circuits, intensify with HIIT, and pedal towards vitality with cycling sessions

                    4. Wellness in The Meru Sanur
                        Welcome to our tranquil haven, dedicated to nurturing the harmony of mind, body, and spirit.
                        Join us on a transformative wellness voyage, where our locally inspired services engage all five senses—sound, sight, scent, taste, and touch—ensuring a holistic and revitalizing journe

                    5. Resort Swimming Pool
                        Surrounded by lush tropical gardens and offering breathtaking views of the Sanur Beach, our impressive swimming pool provides an ideal setting to swim laps or a leisurely, refreshing dip at your convenience

                    6. Kids Club
                        Our kids club enhances the family-friendly atmosphere at The Meru Sanur, making it an ideal destination for quality time with your loved ones

                    7. Fitness Center
                        Our fitness center features advanced equipment, providing you with access to the latest tools for your workout needs. Operating 24 hours a day, our fitness facility is designed to accommodate your schedule, allowing you the flexibility to exercise at your convenience.

                    8. Sanur Beach
                        Take a leisurely stroll along the pristine white sands of Sanur Beach and witness the breathtaking sunrise unfold before your eyes. An unforgettable start to your day awaits.

                    9. Arunika Restaurant
                        Set in the midst of lan expansive pool within the main gardens, Arunika features live cooking stations with an extensive array of local and international cuisines. It serves as the perfect setting for day-long relaxation, creating an ideal space to unwind with friends and family.

                    10. Sutasoma Lounge
                        Discover serenity at Sutasoma Lounge, a haven where history meets relaxation. Indulge in coffee, tea, and a welcoming Jamu. This intimate space doubles as a library, featuring curated books on Bali, Indonesia’s history, and more—an ideal retreat for every guest.

                    11. In Dinning
                        Elevate your in-room dining experience at The Meru Sanur with a delectable array of international and healthy cuisine options, bringing the taste of luxury right to your doorstep.
                        
        4.  question: apa fasilitas kamar Meru Suite?  
            answer: Amenities
                      Work desk with high-speed broadband internet connection Main Room
                      Experience our Premier Suites, feature 125 square meters of space and a stunning view of our tranquil garden. Each suite is adorned with refined furnishings includes a private balcony or terrace, providing the ultimate of luxurious resort-style living.	
                      ⦁	Direct dial telephones with two lines and speakerphone
                      ⦁	65-inch Smart LED TV
                      ⦁	Alarm bluetooth speaker
                      ⦁	In-room safety deposit box
                      ⦁	Full length mirror
                      ⦁	Hair dryer
                      ⦁	Minibar, coffee & tea maker, espresso machine
                      ⦁	Complimentary bottled water
                      ⦁	Reading lamp
                      ⦁	Iron & iron board
                      ⦁	Umbrella
                      ⦁	Slippers
                      ⦁	Yukata

                    Inclusions
                      ⦁	Daily Breakfast for 2 persons
                      ⦁	Butler Service
        
        5.  question: berapa harga kamarnya?
            answer: Ada beberapa jenis kamar yang tersedia di Hotel Meru Sanur
                    1. Meru Suite
                      Kenyamanan yang luas dari Meru Suite kami, menampilkan ruang tamu terpisah dengan teras atau balkon yang menghadap ke Pantai Sanur yang tenang dan taman. Dengan luas 160 meter persegi, suite ini menawarkan pengalaman tinggal yang luar biasa.
                      dengan harga RP 16,385,000 
                    2. Premier Suite
                      Rasakan pengalaman menginap di Premier Suites kami, dengan luas 125 meter persegi dan pemandangan taman kami yang tenang. Setiap kamar dihiasi dengan perabotan mewah termasuk balkon atau teras pribadi, memberikan kehidupan mewah bergaya resor.
                      dengan harga RP 8,700,000 
                    3. Deluxe Suite
                      Rasakan kemewahan Deluxe Suite kami, tempat peristirahatan seluas 83 meter persegi yang luas dengan balkon pribadi atau teras yang luas yang menghadap ke pantai Sanur yang tenang atau taman tropis.
                      dengan harga RP 5,974,000 
                    4. Corner Suite
                      Manjakan diri Anda dengan kenyamanan yang luas di Corner Suite kami, dengan pemandangan taman yang tenang. Menawarkan luas 98 meter persegi, suite ini memiliki balkon atau teras pribadi dan meja makan khusus untuk kenyamanan Anda.      
                      dengan harga RP 6,670,000
                    5. Presidential Suite
                      Rasakan kemewahan dengan Presidential Suites kami, akomodasi paling bergengsi di resor ini. Kamar seluas 209 meter persegi ini menjanjikan tempat peristirahatan yang luar biasa, memastikan pengalaman yang tak tertandingi.    
                      dengan harga RP 9,000,000 

        6.  question: ada kamar apa hari ini?
            answer: Untuk hari ini ada beberapa jenis kamar yang tersedia
                    1. Meru Suite
                      Kenyamanan yang luas dari Meru Suite kami, menampilkan ruang tamu terpisah dengan teras atau balkon yang menghadap ke Pantai Sanur yang tenang dan taman. Dengan luas 160 meter persegi, suite ini menawarkan pengalaman tinggal yang luar biasa.
                      dengan harga RP 16,385,000 
                    2. Premier Suite
                      Rasakan pengalaman menginap di Premier Suites kami, dengan luas 125 meter persegi dan pemandangan taman kami yang tenang. Setiap kamar dihiasi dengan perabotan mewah termasuk balkon atau teras pribadi, memberikan kehidupan mewah bergaya resor.
                      dengan harga RP 8,700,000 
                    3. Deluxe Suite
                      Rasakan kemewahan Deluxe Suite kami, tempat peristirahatan seluas 83 meter persegi yang luas dengan balkon pribadi atau teras yang luas yang menghadap ke pantai Sanur yang tenang atau taman tropis.
                      dengan harga RP 5,974,000 
                    4. Corner Suite
                      Manjakan diri Anda dengan kenyamanan yang luas di Corner Suite kami, dengan pemandangan taman yang tenang. Menawarkan luas 98 meter persegi, suite ini memiliki balkon atau teras pribadi dan meja makan khusus untuk kenyamanan Anda.      
                      dengan harga RP 6,670,000
        </Example>
`;

  const chatPrompt1 = ChatPromptTemplate.fromMessages([
    ["system", prompt],
    new MessagesPlaceholder("chat_history"),
    ["human", "{input}"],
    new MessagesPlaceholder("agent_scratchpad"),
  ]);

  const db = await SqlDatabase.fromDataSourceParams({
    appDataSource: pgDataSource,
  });
  const toolkit = new SqlToolkit(db, model);

  const tools = [
    new DynamicTool({
      name: "Retriever",
      description: "use this tool to retrieve document for the context.",
      func: async (input) => {
        const retriever = MultiQueryRetriever.fromLLM({
          llm: model,
          retriever: vectorStore.asRetriever(),
        });
        const result = await retriever.getRelevantDocuments(input);
        const resultString = result.map((doc) => doc.pageContent).join("\n");
        return resultString;
      },
    }),
    ...toolkit.getTools(),
    // new DynamicTool({
    //   name: "Retriever database",
    //   description: "use this tool to get booking list from database.",
    //   func: async (input) => {
    //     const db = await SqlDatabase.fromDataSourceParams({
    //       appDataSource: pgDataSource,
    //     });
    //     const toolkit = new SqlToolkit(db, model);
    //     const executor = createSqlAgent(model, toolkit);
    //     const result = await executor.invoke({ input });
    //     // console.log(result);
    //     await pgDataSource.destroy();
    //     return result.output;
    //   },
    // }),
  ];

  const agent = await createOpenAIFunctionsAgent({
    llm: model,
    prompt: chatPrompt1,
    tools,
  });

  const agentExecutor = new AgentExecutor({
    agent,
    tools,
    // verbose:true
  }).withConfig({
    runName: "MeruHotelAssistant",
  });

  return agentExecutor;
}

export default async function createChain(chatHistory: BaseChatMessageHistory) {
  const memory = new BufferWindowMemory({
    k: 5,
    chatHistory,
    returnMessages: true,
    inputKey: "input",
    outputKey: "output",
  });

  const agentExecutor = await createMeruHotelAgent(chatHistory);

  const chain2 = RunnableSequence.from([
    {
      input: ({ input }) => {
        return input;
      },
      chat_history: ({ chat_history }) => {
        return chat_history;
      },
      agent_scratchpad: () => "",
    },
    agentExecutor,
  ]);

  const chain3 = RunnableSequence.from([
    {
      input: (initialInput) => initialInput.input,
      chat_history: (initialInput) => initialInput.memory.history,
    },
    chain2,
  ]);

  const chain: RunnableSequence<any, any> = RunnableSequence.from([
    {
      input: (initialInput) => initialInput.input,
      memory: () => memory.loadMemoryVariables({}),
    },
    chain3,
  ]);

  return chain;
}

