import { DataSource } from "typeorm";

export const pgDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "Akmaldira123",
  database: "chatbot_hotel",
});

export const setupDb = async (): Promise<void> => {
  try {
    if (!pgDataSource.isInitialized) {
      await pgDataSource.initialize();
    }
  } catch (err: unknown) {
    throw err;
  }
};

