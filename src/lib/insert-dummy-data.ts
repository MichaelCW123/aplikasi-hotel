import prisma from "@/lib/prisma";
import { faker } from "@faker-js/faker";
import { readFileSync } from "fs";

function createRandomUser() {
  return {
    guestName: faker.person.fullName(),
    guestPhone: faker.phone.number(),
    guestEmail: faker.internet.email(),
    guestNote: undefined,
  };
}

export async function insertDummyData() {
  const stringData = readFileSync("hotel.json");
  const data = JSON.parse(stringData.toString())[0] as {
    roomType: {
      id: number;
      name: string;
      capacity: number;
      description: string;
    }[];
    room: {
      id: number;
      roomNumber: number;
      price: number;
      roomTypeId: number;
      status: string;
    }[];
    bookingList: {
      id: number;
      roomId: number;
      startDate: string;
      endDate: string;
    }[];
  };

  for (const roomType of data.roomType) {
    const roomPrice =
      data.room.find((r) => r.roomTypeId == roomType.id)?.price || 0;

    await prisma.roomType.upsert({
      where: {
        id: roomType.id,
      },
      create: {
        name: roomType.name,
        totalRoom: roomType.capacity,
        dailyPrice: roomPrice,
        description: roomType.description,
      },
      update: {},
    });
  }

  for (const room of data.room) {
    await prisma.room.upsert({
      where: {
        id: room.id,
      },
      create: {
        number: room.roomNumber.toString(),
        maxGuests: 2,
        type: {
          connect: {
            id: room.roomTypeId,
          },
        },
      },
      update: {},
    });
  }

  for (const booking of data.bookingList) {
    const user = createRandomUser();
    await prisma.booking.upsert({
      where: {
        id: booking.id,
      },
      create: {
        room: {
          connect: {
            id: booking.roomId,
          },
        },
        checkIn: new Date(booking.startDate),
        checkOut: new Date(booking.endDate),
        status: "CONFIRMED",
        guestName: user.guestName,
        guestEmail: user.guestEmail,
        guestPhone: user.guestPhone,
        guestNote: user.guestNote,
      },
      update: {},
    });
  }
}

