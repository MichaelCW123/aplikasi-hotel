/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        hostname: "themerusanur.com",
      },
    ],
  },
  reactStrictMode: false,
};

export default nextConfig;

